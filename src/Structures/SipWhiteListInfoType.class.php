<?php
/**
 * Generated by irbisadm
 * generator v.0.0.1-alpha-1
 * at 05.05.2016 13:52:02
**/

namespace Irbisadm\VIHTTP\Structures;

class SipWhiteListInfoType implements JsonSerializable{

  /** @var number The SIP white list item ID. */
  private $_sip_whitelist_id;

  /** 
   * The SIP white list item ID.
   * @return number
  **/
  public function getSipWhitelistId(){
    return $this->_sip_whitelist_id;
  }

  /** 
   * The SIP white list item ID.
   * @param number $sipWhitelistId The SIP white list item ID.
  **/
  public function setSipWhitelistId($sipWhitelistId){
    $this->_sip_whitelist_id = $sipWhitelistId;
  }

  /** @var string The network address in format A.B.C.D/L */
  private $_sip_whitelist_network;

  /** 
   * The network address in format A.B.C.D/L
   * @return string
  **/
  public function getSipWhitelistNetwork(){
    return $this->_sip_whitelist_network;
  }

  /** 
   * The network address in format A.B.C.D/L
   * @param string $sipWhitelistNetwork The network address in format A.B.C.D/L
  **/
  public function setSipWhitelistNetwork($sipWhitelistNetwork){
    $this->_sip_whitelist_network = $sipWhitelistNetwork;
  }

  public function jsonSerialize(){
    return [
    'sip_whitelist_id' => $this->_sip_whitelist_id,
    'sip_whitelist_network' => $this->_sip_whitelist_network
    ];
  }

  public function fromArray($data){
    if(!empty($data['sip_whitelist_id']))
      $this->_sip_whitelist_id = $data['sip_whitelist_id'];
    if(!empty($data['sip_whitelist_network']))
      $this->_sip_whitelist_network = $data['sip_whitelist_network'];
    return $this;
  }

}