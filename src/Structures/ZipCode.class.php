<?php
/**
 * Generated by irbisadm
 * generator v.0.0.1-alpha-1
 * at 05.05.2016 13:52:02
**/

namespace Irbisadm\VIHTTP\Structures;

class ZipCode implements JsonSerializable{

  /** @var string The city name */
  private $_city;

  /** 
   * The city name
   * @return string
  **/
  public function getCity(){
    return $this->_city;
  }

  /** 
   * The city name
   * @param string $city The city name
  **/
  public function setCity($city){
    $this->_city = $city;
  }

  /** @var string The zip code */
  private $_zip_code;

  /** 
   * The zip code
   * @return string
  **/
  public function getZipCode(){
    return $this->_zip_code;
  }

  /** 
   * The zip code
   * @param string $zipCode The zip code
  **/
  public function setZipCode($zipCode){
    $this->_zip_code = $zipCode;
  }

  public function jsonSerialize(){
    return [
    'city' => $this->_city,
    'zip_code' => $this->_zip_code
    ];
  }

  public function fromArray($data){
    if(!empty($data['city']))
      $this->_city = $data['city'];
    if(!empty($data['zip_code']))
      $this->_zip_code = $data['zip_code'];
    return $this;
  }

}