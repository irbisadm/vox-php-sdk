<?php
/**
 * Generated by irbisadm
 * generator v.0.0.1-alpha-1
 * at 05.05.2016 13:52:02
**/

namespace Irbisadm\VIHTTP\Structures;

class HistoryReportType implements JsonSerializable{

  /** @var number The call history report ID. */
  private $_history_report_id;

  /** 
   * The call history report ID.
   * @return number
  **/
  public function getHistoryReportId(){
    return $this->_history_report_id;
  }

  /** 
   * The call history report ID.
   * @param number $historyReportId The call history report ID.
  **/
  public function setHistoryReportId($historyReportId){
    $this->_history_report_id = $historyReportId;
  }

  /** @var string The history report type. The following values are possible: calls, transactions */
  private $_history_type;

  /** 
   * The history report type. The following values are possible: calls, transactions
   * @return string
  **/
  public function getHistoryType(){
    return $this->_history_type;
  }

  /** 
   * The history report type. The following values are possible: calls, transactions
   * @param string $historyType The history report type. The following values are possible: calls, transactions
  **/
  public function setHistoryType($historyType){
    $this->_history_type = $historyType;
  }

  /** @var string The UTC account created time in format: YYYY-MM-DD HH:mm:SS */
  private $_created;

  /** 
   * The UTC account created time in format: YYYY-MM-DD HH:mm:SS
   * @return string
  **/
  public function getCreated(){
    return $this->_created;
  }

  /** 
   * The UTC account created time in format: YYYY-MM-DD HH:mm:SS
   * @param string $created The UTC account created time in format: YYYY-MM-DD HH:mm:SS
  **/
  public function setCreated($created){
    $this->_created = $created;
  }

  /** @var string The report format type. The following values are possible: csv */
  private $_format;

  /** 
   * The report format type. The following values are possible: csv
   * @return string
  **/
  public function getFormat(){
    return $this->_format;
  }

  /** 
   * The report format type. The following values are possible: csv
   * @param string $format The report format type. The following values are possible: csv
  **/
  public function setFormat($format){
    $this->_format = $format;
  }

  /** @var string The UTC completion time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists. */
  private $_completed;

  /** 
   * The UTC completion time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
   * @return string
  **/
  public function getCompleted(){
    return $this->_completed;
  }

  /** 
   * The UTC completion time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
   * @param string $completed The UTC completion time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
  **/
  public function setCompleted($completed){
    $this->_completed = $completed;
  }

  /** @var string The report file name. */
  private $_file_name;

  /** 
   * The report file name.
   * @return string
  **/
  public function getFileName(){
    return $this->_file_name;
  }

  /** 
   * The report file name.
   * @param string $fileName The report file name.
  **/
  public function setFileName($fileName){
    $this->_file_name = $fileName;
  }

  /** @var number The report file size. */
  private $_file_size;

  /** 
   * The report file size.
   * @return number
  **/
  public function getFileSize(){
    return $this->_file_size;
  }

  /** 
   * The report file size.
   * @param number $fileSize The report file size.
  **/
  public function setFileSize($fileSize){
    $this->_file_size = $fileSize;
  }

  /** @var number The gzipped report size to download. */
  private $_download_size;

  /** 
   * The gzipped report size to download.
   * @return number
  **/
  public function getDownloadSize(){
    return $this->_download_size;
  }

  /** 
   * The gzipped report size to download.
   * @param number $downloadSize The gzipped report size to download.
  **/
  public function setDownloadSize($downloadSize){
    $this->_download_size = $downloadSize;
  }

  /** @var number The download attempt count. */
  private $_download_count;

  /** 
   * The download attempt count.
   * @return number
  **/
  public function getDownloadCount(){
    return $this->_download_count;
  }

  /** 
   * The download attempt count.
   * @param number $downloadCount The download attempt count.
  **/
  public function setDownloadCount($downloadCount){
    $this->_download_count = $downloadCount;
  }

  /** @var string The last download UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists. */
  private $_last_downloaded;

  /** 
   * The last download UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
   * @return string
  **/
  public function getLastDownloaded(){
    return $this->_last_downloaded;
  }

  /** 
   * The last download UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
   * @param string $lastDownloaded The last download UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
  **/
  public function setLastDownloaded($lastDownloaded){
    $this->_last_downloaded = $lastDownloaded;
  }

  /** @var string Store the report until the UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists. */
  private $_store_until;

  /** 
   * Store the report until the UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
   * @return string
  **/
  public function getStoreUntil(){
    return $this->_store_until;
  }

  /** 
   * Store the report until the UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
   * @param string $storeUntil Store the report until the UTC time in format: YYYY-MM-DD HH:mm:SS. The report is completed if the field exists.
  **/
  public function setStoreUntil($storeUntil){
    $this->_store_until = $storeUntil;
  }

  /** @var number The session count in the report. */
  private $_session_count;

  /** 
   * The session count in the report.
   * @return number
  **/
  public function getSessionCount(){
    return $this->_session_count;
  }

  /** 
   * The session count in the report.
   * @param number $sessionCount The session count in the report.
  **/
  public function setSessionCount($sessionCount){
    $this->_session_count = $sessionCount;
  }

  /** @var number The total found filtered session count. */
  private $_total_session_count;

  /** 
   * The total found filtered session count.
   * @return number
  **/
  public function getTotalSessionCount(){
    return $this->_total_session_count;
  }

  /** 
   * The total found filtered session count.
   * @param number $totalSessionCount The total found filtered session count.
  **/
  public function setTotalSessionCount($totalSessionCount){
    $this->_total_session_count = $totalSessionCount;
  }

  /** @var API_Error The report error. */
  private $_error;

  /** 
   * The report error.
   * @return API_Error
  **/
  public function getError(){
    return $this->_error;
  }

  /** 
   * The report error.
   * @param API_Error $error The report error.
  **/
  public function setError($error){
    $this->_error = $error;
  }

  /** @var Object The report order filters (the saved GetCallHistory, GetTransactionHistory parameters). */
  private $_filters;

  /** 
   * The report order filters (the saved GetCallHistory, GetTransactionHistory parameters).
   * @return Object
  **/
  public function getFilters(){
    return $this->_filters;
  }

  /** 
   * The report order filters (the saved GetCallHistory, GetTransactionHistory parameters).
   * @param Object $filters The report order filters (the saved GetCallHistory, GetTransactionHistory parameters).
  **/
  public function setFilters($filters){
    $this->_filters = $filters;
  }

  /** @var Object The calculated report data (the specific report data, see CalculatedCallHistoryDataType, CalculatedTransactionHistoryDataType). */
  private $_calculated_data;

  /** 
   * The calculated report data (the specific report data, see CalculatedCallHistoryDataType, CalculatedTransactionHistoryDataType).
   * @return Object
  **/
  public function getCalculatedData(){
    return $this->_calculated_data;
  }

  /** 
   * The calculated report data (the specific report data, see CalculatedCallHistoryDataType, CalculatedTransactionHistoryDataType).
   * @param Object $calculatedData The calculated report data (the specific report data, see CalculatedCallHistoryDataType, CalculatedTransactionHistoryDataType).
  **/
  public function setCalculatedData($calculatedData){
    $this->_calculated_data = $calculatedData;
  }

  public function jsonSerialize(){
    return [
    'history_report_id' => $this->_history_report_id,
    'history_type' => $this->_history_type,
    'created' => $this->_created,
    'format' => $this->_format,
    'completed' => $this->_completed,
    'file_name' => $this->_file_name,
    'file_size' => $this->_file_size,
    'download_size' => $this->_download_size,
    'download_count' => $this->_download_count,
    'last_downloaded' => $this->_last_downloaded,
    'store_until' => $this->_store_until,
    'session_count' => $this->_session_count,
    'total_session_count' => $this->_total_session_count,
    'error' => $this->_error,
    'filters' => $this->_filters,
    'calculated_data' => $this->_calculated_data
    ];
  }

  public function fromArray($data){
    if(!empty($data['history_report_id']))
      $this->_history_report_id = $data['history_report_id'];
    if(!empty($data['history_type']))
      $this->_history_type = $data['history_type'];
    if(!empty($data['created']))
      $this->_created = $data['created'];
    if(!empty($data['format']))
      $this->_format = $data['format'];
    if(!empty($data['completed']))
      $this->_completed = $data['completed'];
    if(!empty($data['file_name']))
      $this->_file_name = $data['file_name'];
    if(!empty($data['file_size']))
      $this->_file_size = $data['file_size'];
    if(!empty($data['download_size']))
      $this->_download_size = $data['download_size'];
    if(!empty($data['download_count']))
      $this->_download_count = $data['download_count'];
    if(!empty($data['last_downloaded']))
      $this->_last_downloaded = $data['last_downloaded'];
    if(!empty($data['store_until']))
      $this->_store_until = $data['store_until'];
    if(!empty($data['session_count']))
      $this->_session_count = $data['session_count'];
    if(!empty($data['total_session_count']))
      $this->_total_session_count = $data['total_session_count'];
    if(!empty($data['error']))
      $this->_error = $data['error'];
    if(!empty($data['filters']))
      $this->_filters = $data['filters'];
    if(!empty($data['calculated_data']))
      $this->_calculated_data = $data['calculated_data'];
    return $this;
  }

}