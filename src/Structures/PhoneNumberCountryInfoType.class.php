<?php
/**
 * Generated by irbisadm
 * generator v.0.0.1-alpha-1
 * at 05.05.2016 13:52:02
**/

namespace Irbisadm\VIHTTP\Structures;

class PhoneNumberCountryInfoType implements JsonSerializable{

  /** @var string The country code. */
  private $_country_code;

  /** 
   * The country code.
   * @return string
  **/
  public function getCountryCode(){
    return $this->_country_code;
  }

  /** 
   * The country code.
   * @param string $countryCode The country code.
  **/
  public function setCountryCode($countryCode){
    $this->_country_code = $countryCode;
  }

  /** @var string The country phone prefix. */
  private $_phone_prefix;

  /** 
   * The country phone prefix.
   * @return string
  **/
  public function getPhonePrefix(){
    return $this->_phone_prefix;
  }

  /** 
   * The country phone prefix.
   * @param string $phonePrefix The country phone prefix.
  **/
  public function setPhonePrefix($phonePrefix){
    $this->_phone_prefix = $phonePrefix;
  }

  /** @var boolean True if can list phone numbers. */
  private $_can_list_phone_numbers;

  /** 
   * True if can list phone numbers.
   * @return boolean
  **/
  public function getCanListPhoneNumbers(){
    return $this->_can_list_phone_numbers;
  }

  /** 
   * True if can list phone numbers.
   * @param boolean $canListPhoneNumbers True if can list phone numbers.
  **/
  public function setCanListPhoneNumbers($canListPhoneNumbers){
    $this->_can_list_phone_numbers = $canListPhoneNumbers;
  }

  /** @var [PhoneNumberCountryCategoryInfoType] The phone categories. */
  private $_phone_categories;

  /** 
   * The phone categories.
   * @return [PhoneNumberCountryCategoryInfoType]
  **/
  public function getPhoneCategories(){
    return $this->_phone_categories;
  }

  /** 
   * The phone categories.
   * @param [PhoneNumberCountryCategoryInfoType] $phoneCategories The phone categories.
  **/
  public function setPhoneCategories($phoneCategories){
    $this->_phone_categories = $phoneCategories;
  }

  public function jsonSerialize(){
    return [
    'country_code' => $this->_country_code,
    'phone_prefix' => $this->_phone_prefix,
    'can_list_phone_numbers' => $this->_can_list_phone_numbers,
    'phone_categories' => $this->_phone_categories
    ];
  }

  public function fromArray($data){
    if(!empty($data['country_code']))
      $this->_country_code = $data['country_code'];
    if(!empty($data['phone_prefix']))
      $this->_phone_prefix = $data['phone_prefix'];
    if(!empty($data['can_list_phone_numbers']))
      $this->_can_list_phone_numbers = $data['can_list_phone_numbers'];
    if(!empty($data['phone_categories']))
      $this->_phone_categories = $data['phone_categories'];
    return $this;
  }

}