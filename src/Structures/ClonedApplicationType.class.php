<?php
/**
 * Generated by irbisadm
 * generator v.0.0.1-alpha-1
 * at 05.05.2016 13:52:02
**/

namespace Irbisadm\VIHTTP\Structures;

class ClonedApplicationType implements JsonSerializable{

  /** @var number The application ID. */
  private $_application_id;

  /** 
   * The application ID.
   * @return number
  **/
  public function getApplicationId(){
    return $this->_application_id;
  }

  /** 
   * The application ID.
   * @param number $applicationId The application ID.
  **/
  public function setApplicationId($applicationId){
    $this->_application_id = $applicationId;
  }

  /** @var string The full application name. */
  private $_application_name;

  /** 
   * The full application name.
   * @return string
  **/
  public function getApplicationName(){
    return $this->_application_name;
  }

  /** 
   * The full application name.
   * @param string $applicationName The full application name.
  **/
  public function setApplicationName($applicationName){
    $this->_application_name = $applicationName;
  }

  /** @var [ClonedRuleType] The cloned rules. */
  private $_users;

  /** 
   * The cloned rules.
   * @return [ClonedRuleType]
  **/
  public function getUsers(){
    return $this->_users;
  }

  /** 
   * The cloned rules.
   * @param [ClonedRuleType] $users The cloned rules.
  **/
  public function setUsers($users){
    $this->_users = $users;
  }

  public function jsonSerialize(){
    return [
    'application_id' => $this->_application_id,
    'application_name' => $this->_application_name,
    'users' => $this->_users
    ];
  }

  public function fromArray($data){
    if(!empty($data['application_id']))
      $this->_application_id = $data['application_id'];
    if(!empty($data['application_name']))
      $this->_application_name = $data['application_name'];
    if(!empty($data['users']))
      $this->_users = $data['users'];
    return $this;
  }

}