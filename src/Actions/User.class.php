<?php
/**
 * Generated by irbisadm
 * generator v.0.0.1-alpha-1
 * at 05.05.2016 13:52:02
**/

namespace Irbisadm\VIHTTP\Actions;

class User {

  /**
   * Adds a new user.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param string $user_name The user name in format [a-z0-9][a-z0-9_-]{2,49}
   * @param string $user_display_name The user display name. The length must be less than 256.
   * @param string $user_password The user password. The length must be at least 6 symbols.
   * @param boolean $parent_accounting Is account money use?
   * @param boolean $two_factor_auth_required Is two factor authorization required?
   * @param string $mobile_phone The user mobile phone. The length must be less than 50.
   * @param boolean $user_active The user enable flag
   * @param string $user_custom_data Any string
  **/
  public function AddUser($user_custom_data,$user_active,$mobile_phone,$two_factor_auth_required,$parent_accounting,$auth,$user_name = null,$user_display_name = null,$user_password = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($user_name))$
      $form_params['user_name'] = $user_name
    if(!empty($user_display_name))$
      $form_params['user_display_name'] = $user_display_name
    if(!empty($user_password))$
      $form_params['user_password'] = $user_password
    if(!empty($parent_accounting))$
      $form_params['parent_accounting'] = $parent_accounting
    if(!empty($two_factor_auth_required))$
      $form_params['two_factor_auth_required'] = $two_factor_auth_required
    if(!empty($mobile_phone))$
      $form_params['mobile_phone'] = $mobile_phone
    if(!empty($user_active))$
      $form_params['user_active'] = $user_active
    if(!empty($user_custom_data))$
      $form_params['user_custom_data'] = $user_custom_data
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    $parsedResponse['user_id'] = $rawResponse['user_id'];
    return $parsedResponse;
    
  }

  /**
   * Deletes the user.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param string $user_id The user ID list separated by the ';' symbol or the 'all' value.
   * @param string $user_name The user name list separated by the ';' symbol that can be used instead of <b>user_id</b>.
  **/
  public function DelUser($auth,$user_id = null,$user_name = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($user_id))$
      $form_params['user_id'] = $user_id
    if(!empty($user_name))$
      $form_params['user_name'] = $user_name
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    return $parsedResponse;
    
  }

  /**
   * Edits the user.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param number $user_id The user to edit.
   * @param string $user_name Can be used instead of <b>user_id</b>.
   * @param string $new_user_name The new user name in format [a-z0-9][a-z0-9_-]{2,49}
   * @param string $user_display_name The new user display name. The length must be less than 256.
   * @param string $user_password The new user password. The length must be at least 6 symbols.
   * @param boolean $parent_accounting Is account money use?
   * @param boolean $user_active The user enable flag
   * @param string $user_custom_data Any string
   * @param boolean $two_factor_auth_required Is two factor authorization required?
   * @param string $mobile_phone The new user mobile phone. The length must be less than 50.
  **/
  public function SetUserInfo($mobile_phone,$two_factor_auth_required,$user_custom_data,$user_active,$parent_accounting,$user_password,$user_display_name,$new_user_name,$auth,$user_id = null,$user_name = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($user_id))$
      $form_params['user_id'] = $user_id
    if(!empty($user_name))$
      $form_params['user_name'] = $user_name
    if(!empty($new_user_name))$
      $form_params['new_user_name'] = $new_user_name
    if(!empty($user_display_name))$
      $form_params['user_display_name'] = $user_display_name
    if(!empty($user_password))$
      $form_params['user_password'] = $user_password
    if(!empty($parent_accounting))$
      $form_params['parent_accounting'] = $parent_accounting
    if(!empty($user_active))$
      $form_params['user_active'] = $user_active
    if(!empty($user_custom_data))$
      $form_params['user_custom_data'] = $user_custom_data
    if(!empty($two_factor_auth_required))$
      $form_params['two_factor_auth_required'] = $two_factor_auth_required
    if(!empty($mobile_phone))$
      $form_params['mobile_phone'] = $mobile_phone
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    return $parsedResponse;
    
  }

  /**
   * Bind the user list with the application list.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param string $user_id The user ID list separated by the ';' symbol or the 'all' value.
   * @param string $user_name Can be used instead of <b>user_id</b>. The user name list separated by the ';' symbol.
   * @param string $application_id The application ID list separated by the ';' symbol or the 'all' value.
   * @param string $application_name Can be used instead of <b>application_id</b>. The application name list separated by the ';' symbol.
   * @param boolean $bind Bind or unbind?
  **/
  public function BindUser($bind,$auth,$user_id = null,$user_name = null,$application_id = null,$application_name = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($user_id))$
      $form_params['user_id'] = $user_id
    if(!empty($user_name))$
      $form_params['user_name'] = $user_name
    if(!empty($application_id))$
      $form_params['application_id'] = $application_id
    if(!empty($application_name))$
      $form_params['application_name'] = $application_name
    if(!empty($bind))$
      $form_params['bind'] = $bind
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    return $parsedResponse;
    
  }

  /**
   * Gets the users.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param number $application_id The application ID to filter.
   * @param number $excluded_application_id The excluded application ID to filter.
   * @param string $excluded_application_name The excluded application name to filter.
   * @param number $skill_id The skill ID to filter.
   * @param number $excluded_skill_id The excluded skill ID to filter.
   * @param number $acd_queue_id The ACD queue ID to filter.
   * @param number $user_id The user ID to filter.
   * @param string $user_name The user name part to filter.
   * @param boolean $user_active The user active flag to filter.
   * @param string $user_display_name The user display name part to filter.
   * @param string $application_name The application name part to filter.
   * @param boolean $with_applications Set true to get the bound applications.
   * @param boolean $with_skills Set true to get the bound skills.
   * @param boolean $with_queues Set true to get the bound queues.
   * @param string $acd_status The ACD status list separated by the ';' symbol.
   * @param number $showing_application_id The application to show in the 'applications' field output.
   * @param number $showing_skill_id The skill to show in the 'skills' field output.
   * @param number $count The max returning record count.
   * @param number $offset The record count to omit.
   * @param string $order_by The following values are available: 'user_id', 'user_name' and 'user_display_name'.
   * @param boolean $return_live_balance Set true to get the user live balance.
  **/
  public function GetUsers($return_live_balance,$order_by,$offset,$count,$showing_skill_id,$showing_application_id,$acd_status,$with_queues,$with_skills,$with_applications,$application_name,$user_display_name,$user_active,$user_name,$user_id,$acd_queue_id,$excluded_skill_id,$skill_id,$excluded_application_name,$excluded_application_id,$application_id,$auth){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($application_id))$
      $form_params['application_id'] = $application_id
    if(!empty($excluded_application_id))$
      $form_params['excluded_application_id'] = $excluded_application_id
    if(!empty($excluded_application_name))$
      $form_params['excluded_application_name'] = $excluded_application_name
    if(!empty($skill_id))$
      $form_params['skill_id'] = $skill_id
    if(!empty($excluded_skill_id))$
      $form_params['excluded_skill_id'] = $excluded_skill_id
    if(!empty($acd_queue_id))$
      $form_params['acd_queue_id'] = $acd_queue_id
    if(!empty($user_id))$
      $form_params['user_id'] = $user_id
    if(!empty($user_name))$
      $form_params['user_name'] = $user_name
    if(!empty($user_active))$
      $form_params['user_active'] = $user_active
    if(!empty($user_display_name))$
      $form_params['user_display_name'] = $user_display_name
    if(!empty($application_name))$
      $form_params['application_name'] = $application_name
    if(!empty($with_applications))$
      $form_params['with_applications'] = $with_applications
    if(!empty($with_skills))$
      $form_params['with_skills'] = $with_skills
    if(!empty($with_queues))$
      $form_params['with_queues'] = $with_queues
    if(!empty($acd_status))$
      $form_params['acd_status'] = $acd_status
    if(!empty($showing_application_id))$
      $form_params['showing_application_id'] = $showing_application_id
    if(!empty($showing_skill_id))$
      $form_params['showing_skill_id'] = $showing_skill_id
    if(!empty($count))$
      $form_params['count'] = $count
    if(!empty($offset))$
      $form_params['offset'] = $offset
    if(!empty($order_by))$
      $form_params['order_by'] = $order_by
    if(!empty($return_live_balance))$
      $form_params['return_live_balance'] = $return_live_balance
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    foreach($rawResponse['result'] as $item){
      $element = new UserInfoType();
      $parsedResponse['result'][] = $element->fromArray($item);
    }
    $parsedResponse['total_count'] = $rawResponse['total_count'];
    $parsedResponse['count'] = $rawResponse['count'];
    return $parsedResponse;
    
  }

  /**
   * Transfer the account's money to the user or transfer the user's money to the account if the money amount is negative.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param string $user_id The user ID list separated by the ';' symbol.
   * @param number $amount The money amount, $. The absolute amount value must be equal or greater than 0.01
   * @param string $currency The amount currency. Examples: RUR, EUR, USD.
   * @param boolean $strict_mode Returns error if strict_mode is true and an user or the account hasn't enough money.
   * @param string $user_transaction_description The user transaction description.
   * @param string $account_transaction_description The account transaction description. The following macro available: ${user_id}, ${user_name}
  **/
  public function TransferMoneyToUser($account_transaction_description,$user_transaction_description,$strict_mode,$auth,$user_id = null,$amount = null,$currency = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($user_id))$
      $form_params['user_id'] = $user_id
    if(!empty($amount))$
      $form_params['amount'] = $amount
    if(!empty($currency))$
      $form_params['currency'] = $currency
    if(!empty($strict_mode))$
      $form_params['strict_mode'] = $strict_mode
    if(!empty($user_transaction_description))$
      $form_params['user_transaction_description'] = $user_transaction_description
    if(!empty($account_transaction_description))$
      $form_params['account_transaction_description'] = $account_transaction_description
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    $parsedResponse['balance'] = $rawResponse['balance'];
    return $parsedResponse;
    
  }


}