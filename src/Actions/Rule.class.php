<?php
/**
 * Generated by irbisadm
 * generator v.0.0.1-alpha-1
 * at 05.05.2016 13:52:02
**/

namespace Irbisadm\VIHTTP\Actions;

class Rule {

  /**
   * Adds a new rule for the application.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param number $application_id The application ID.
   * @param string $application_name The application name, can be used instead of <b>application_id</b>.
   * @param string $rule_name The rule name. The length must be less than 512
   * @param string $rule_pattern The rule pattern regex. The length must be less than 64 KB.
   * @param string $rule_pattern_exclude The exclude pattern regex. The length must be less than 64 KB.
   * @param string $scenario_id The scenario ID list separated by the ';' symbol.
   * @param string $scenario_name Can be used instead of <b>scenario_id</b>. The scenario name list separated by the ';' symbol.
  **/
  public function AddRule($rule_pattern_exclude,$auth,$application_id = null,$application_name = null,$rule_name = null,$rule_pattern = null,$scenario_id = null,$scenario_name = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($application_id))$
      $form_params['application_id'] = $application_id
    if(!empty($application_name))$
      $form_params['application_name'] = $application_name
    if(!empty($rule_name))$
      $form_params['rule_name'] = $rule_name
    if(!empty($rule_pattern))$
      $form_params['rule_pattern'] = $rule_pattern
    if(!empty($rule_pattern_exclude))$
      $form_params['rule_pattern_exclude'] = $rule_pattern_exclude
    if(!empty($scenario_id))$
      $form_params['scenario_id'] = $scenario_id
    if(!empty($scenario_name))$
      $form_params['scenario_name'] = $scenario_name
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    $parsedResponse['rule_id'] = $rawResponse['rule_id'];
    return $parsedResponse;
    
  }

  /**
   * Deletes the rule.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param string $rule_id The rule ID list separated by the ';' symbol or the 'all' value.
   * @param string $rule_name Can be used instead of <b>rule_id</b>. The rule name list separated by the ';' symbol.
   * @param string $application_id The application ID list separated by the ';' symbol or the 'all' value.
   * @param string $application_name Can be used instead of <b>application_id</b>. The application name list separated by the ';' symbol.
  **/
  public function DelRule($auth,$rule_id = null,$rule_name = null,$application_id = null,$application_name = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($rule_id))$
      $form_params['rule_id'] = $rule_id
    if(!empty($rule_name))$
      $form_params['rule_name'] = $rule_name
    if(!empty($application_id))$
      $form_params['application_id'] = $application_id
    if(!empty($application_name))$
      $form_params['application_name'] = $application_name
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    return $parsedResponse;
    
  }

  /**
   * Edits the rule.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param number $rule_id The rule ID.
   * @param string $rule_name The new rule name. The length must be less than 512
   * @param string $rule_pattern The new rule pattern regex. The length must be less than 64 KB.
   * @param string $rule_pattern_exclude The new exclude pattern regex. The length must be less than 64 KB.
  **/
  public function SetRuleInfo($rule_pattern_exclude,$rule_pattern,$rule_name,$auth,$rule_id = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($rule_id))$
      $form_params['rule_id'] = $rule_id
    if(!empty($rule_name))$
      $form_params['rule_name'] = $rule_name
    if(!empty($rule_pattern))$
      $form_params['rule_pattern'] = $rule_pattern
    if(!empty($rule_pattern_exclude))$
      $form_params['rule_pattern_exclude'] = $rule_pattern_exclude
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    return $parsedResponse;
    
  }

  /**
   * Gets the rules.
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param number $application_id The application ID.
   * @param string $application_name The application name that can be used instead of <b>application_id</b>.
   * @param number $rule_id The rule ID to filter
   * @param string $rule_name The rule name part to filter.
   * @param string $template Search for template matching
   * @param boolean $with_scenarios Set true to get binding scenarios info.
   * @param number $count The max returning record count.
   * @param number $offset The record count to omit.
  **/
  public function GetRules($offset,$count,$with_scenarios,$template,$rule_name,$rule_id,$auth,$application_id = null,$application_name = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($application_id))$
      $form_params['application_id'] = $application_id
    if(!empty($application_name))$
      $form_params['application_name'] = $application_name
    if(!empty($rule_id))$
      $form_params['rule_id'] = $rule_id
    if(!empty($rule_name))$
      $form_params['rule_name'] = $rule_name
    if(!empty($template))$
      $form_params['template'] = $template
    if(!empty($with_scenarios))$
      $form_params['with_scenarios'] = $with_scenarios
    if(!empty($count))$
      $form_params['count'] = $count
    if(!empty($offset))$
      $form_params['offset'] = $offset
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    foreach($rawResponse['result'] as $item){
      $element = new RuleInfoType();
      $parsedResponse['result'][] = $element->fromArray($item);
    }
    $parsedResponse['total_count'] = $rawResponse['total_count'];
    $parsedResponse['count'] = $rawResponse['count'];
    return $parsedResponse;
    
  }

  /**
   * Sets the rule selection order. Note: the rules must belong to the same application!
   * @param Irbisadm\VIHTTP\AuthParams $auth parameters Follow link for authentication parameter names and details.
   * @param string $rule_id The rule ID list separated by the ';' symbol.
  **/
  public function ReorderRules($auth,$rule_id = null){
    $form_params = [];
    $form_params = array_merge($form_params,$auth->getAuthArray());
    if(!empty($rule_id))$
      $form_params['rule_id'] = $rule_id
    $client = new GuzzleHttp\Client();
    
    $rez = $client->request('POST', 'https://api.voximplant.com/platform_api/Logon/', [
      'form_params' => $form_params,
      'headers'=>[
        'User-Agent' => 'php-api like guzzle',
        'X-Version'  => '0.0.1',
        'X-Author'   => 'irbisadm'
      ]
    ]);
    $rawResponse = json_decode((string)$rez->getBody(),true);
    $parsedResponse = [];
    $parsedResponse['result'] = $rawResponse['result'];
    return $parsedResponse;
    
  }


}