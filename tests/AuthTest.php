<?php
/**
 * Created by PhpStorm.
 * User: irbisadm
 * Date: 06/05/16
 * Time: 11:12
 */
namespace Irbisadm\VIHTTP;
class AuthTest extends \PHPUnit_Framework_TestCase
{
  public function testNoName()
  {
    $authLogin = new AuthLogin();
    $authPass = new AuthPass();
    $authPass->apiKey = 111;
    new Auth($authLogin, $authPass);
    $this->expectExceptionMessage("Wrong auth");
  }
  public function testNoPass()
  {

    $authLogin = new AuthLogin();
    $authLogin->accountId = 111;
    $authPass = new AuthPass();
    new Auth($authLogin, $authPass);
    $this->expectExceptionMessage("Wrong auth");
  }
}
