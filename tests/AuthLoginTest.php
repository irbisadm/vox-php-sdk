<?php
/**
 * Created by PhpStorm.
 * User: irbisadm
 * Date: 25/05/16
 * Time: 13:19
 */
namespace Irbisadm\VIHTTP;
class AuthLoginTest extends \PHPUnit_Framework_TestCase
{

  public function testIsEmptyTrue()
  {
    $testObject = new AuthLogin();
    $this->assertTrue($testObject->isEmpty());
  }

  public function testIsEmptyFalse()
  {
    $testObject = new AuthLogin();
    $testObject->accountId = '1111';
    $this->assertFalse($testObject->isEmpty());
  }

  public function testGetOneId()
  {
    $testObject = new AuthLogin();
    $testObject->accountId = 'accountId';
    $testObject->accountName = 'accountName';
    $testObject->accountName = 'accountEmail';
    $testObject->adminUserId = 'adminUserId';
    $testObject->adminUserName = 'adminUserName';
    $this->assertArrayHasKey('account_id', $testObject->getOne());
  }
  public function testGetOneName()
  {
    $testObject = new AuthLogin();
    $testObject->accountName = 'accountName';
    $testObject->accountName = 'accountEmail';
    $testObject->adminUserId = 'adminUserId';
    $testObject->adminUserName = 'adminUserName';
    $this->assertArrayHasKey('account_name', $testObject->getOne());
  }
  public function testGetOneEmail()
  {
    $testObject = new AuthLogin();
    $testObject->accountName = 'accountEmail';
    $testObject->adminUserId = 'adminUserId';
    $testObject->adminUserName = 'adminUserName';
    $this->assertArrayHasKey('account_email', $testObject->getOne());
  }
  public function testGetOneAdminId()
  {
    $testObject = new AuthLogin();
    $testObject->accountName = 'accountEmail';
    $testObject->adminUserId = 'adminUserId';
    $testObject->adminUserName = 'adminUserName';
    $this->assertArrayHasKey('admin_user_id', $testObject->getOne());
  }
  public function testGetOneAdminName()
  {
    $testObject = new AuthLogin();
    $testObject->adminUserName = 'adminUserName';
    $this->assertArrayHasKey('admin_user_name', $testObject->getOne());
  }
}
